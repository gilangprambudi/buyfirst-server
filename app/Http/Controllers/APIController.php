<?php
/*
    STATUS Code:
    0 = No Error
    1 = 
    2 = Authentication Failed (Wrong Credential)
    3 = System Failed (Unknown Reason)
    4 = Incorrect Pin Verification


    PAYMENT CODE:
    0: Cash
    1: Kigspay
*/
namespace App\Http\Controllers;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Kigspay as KigspayResource;
use App\Http\Resources\Product as ProductResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

 
use App\User;
use App\Kigspay;
use App\Product;
use App\Transaction;
use App\PurchasedProduct;
use App\Cart;
class APIController extends Controller
{

    /* Route : /authenticate/user   */
    function _AuthenticateUser(Request $req){

        $username = $req->get('username');
        $password = $req->get('password');
        
        $user     = User::where('username', $username)
                          ->orWhere('email', $username)
                          ->orWhere('phone', $username)
                          ->first();
        
        if($user != null){
            if(Hash::check($password, $user->password)){
                $new_token = Str::random(32);
                $user->login_token = $new_token;
                if($user->save()){ 
                    return array("status"=> 0, "user" => new UserResource($user));
                }            
            }else{
                return json_encode(array("status"=> 2));
            }
        }else{
            return json_encode(array("status"=>2));
        }

        return json_encode(array("status"=> 3));
        
    }

    /* Route : /authenticate/user/token */
    function _CheckTokenIntegrity(Request $req){
        $username = $req->get('username');
        $token    = $req->get('token');
        
        $user     = User::where('username', $username)
                          ->orWhere('email', $username)
                          ->orWhere('phone', $username)
                          ->first();
        
        if($user != null){
            if($user->login_token == $token){
                return json_encode(array("status" => 0, "token_match" => true));
            }
            return json_encode(array("status"=>2));
        }else{
            return json_encode(array("status"=>2));
        }
        return json_encode(array("status"=> 3));
    }

    /* Route : /register/user */
    function _RegisterUser(Request $req){
        $username       = $req->get('username');
        $email          = $req->get('email');
        $password       = Hash::make($req->get('password'));

        $user           = User::where('username', $username)->first();

        if($user != null){
            return json_encode(array("status"=> 2, "message" => "Username already used"));
        }

        $user           = User::where('email', $email)->first();
        if($user != null){
            return json_encode(array("status"=> 2, "message" => "Email already used"));
        }

        $user           = new User();

        $user->username = $username;
        $user->email    = $email;
        $user->password = $password;

        $new_token      = Str::random(32);
        $user->login_token = $new_token;

        if($user->save()){
            $kigspay            = new Kigspay();
            $kigspay->user_id   = $user->id;
            $kigspay->email     = $email;
            $kigspay->save();
            return json_encode(array("status"=>0, "message"=>"User Successfuly registered", "user" => new UserResource($user)));
        }else{
            return json_encode(array("status"=> 3));
        }
        return json_encode(array("status"=> 3));
    }

    /* Route : /register/user/verification-pin */
    function _RegisterVerificationPin(Request $req){
        $verification_pin       = $req->get('verification_pin');
        $user_id                = $req->get('user_id');
        $token                  = $req->get('login_token');
        
        $user                   = User::find($user_id);
        
        if($token == $user->login_token){
            $kigspay                   = Kigspay::where('user_id', $user_id)->first();
            $kigspay->verification_pin = Hash::make($verification_pin);
            $kigspay->save();

            return json_encode(array("status"=> 0, "kigspay" => new KigspayResource($kigspay)));
        }else{
            return json_encode(array("status"=> 2));
        }
        return json_encode(array("status"=> 3));  
    }


    /* Route : /authenticate/user/verification_pin */
    function _CheckVerificationPin(Request $req){
        $verification_pin       = $req->get('verification_pin');
        $user_id                = $req->get('user_id');
        $token                  = $req->get('token');
        
        $user                   = User::find($user_id);
        
        if($token == $user->login_token){
            
            $kigspay                   = Kigspay::where('user_id', $user_id)->first();

            if(Hash::check($verification_pin, $kigspay->verification_pin)){
                return json_encode(array("status"=> 0, "pin_correct" => true));
            }

            return json_encode(array("status"=> 2));
        }else{
            return json_encode(array("status"=> 2));
        }
        return json_encode(array("status"=> 3));  
    }

    /*  Route : */
    function _GetKigspayAccount(Request $req){
    
    }

    /* Route : /product/add/cart/qrcode */
    function _AddProductToCartByQRCode(Request $req){
        $qr_code        = $req->get('qr_code');
        $token          = $req->get('token');
        $user_id        = $req->get('user_id');
        $qty            = $req->get('qty');

        $user           = User::find($user_id);

        if($user != null){
            if($token == $user->login_token){
                $product    = Product::where('code', $qr_code)->first();
                $cart       = new Cart();

                if($product != null){
                    
                    $cart->user_id      = $user_id;
                    $cart->product_id   = $product->id;
                    $cart->qty          = $qty;
                    if($cart->save()){
                        return json_encode(array("status"=> 0, "cart" => $cart));
                    }else{
                        return json_encode(array("status"=> 2));
                    }
                }else{
                    return json_encode(array("status"=> 2, "message" => "Product Barcode not found"));
                }

            }else{
                return json_encode(array("status"=> 2));
            }
        }else{
            return json_encode(array("status"=> 2));       
        }

        return json_encode(array("status"=> 3));
    }

    /* Route : /product/get/cart */
    function _GetCart(Request $req){        
        $token          = $req->get('token');
        $user_id        = $req->get('user_id');
        
        $user           = User::find($user_id);


        if($user != null){
            if($token == $user->login_token){
                $carts = Cart::where('user_id', $user_id)->get();
                $total_qty = 0;
                foreach($carts as $cart){
                    $total_qty += $cart->qty;
                }
                return json_encode(array("status" => 0, "cart" => $carts, "cart_qty_total" => $total_qty));
            }else{
                return json_encode(array("status"=> 2, "status" => "Token Mismatch"));
            }
        }else{
            return json_encode(array("status"=> 2));
        }

        return json_encode(array("status"=> 3));
    }

    /* Route : /product/update/cart */
    function _ModifyCart(Request $req){
        $token          = $req->get('token');
        $user_id        = $req->get('user_id');
        $cart_id        = $req->get('cart_id');
        $qty            = $req->get('qty');


        $user           = User::find($user_id);

        if($user != null){
            if($token == $user->login_token){
                $cart = Cart::find($cart_id);
                
                if($cart != null){
                    if($qty == 0){
                        if($cart->delete()){
                            return json_encode(array("status" => 0, "message" => "Cart Deleted"));
                        }
                    }else if($qty > 0){
                        $cart->qty = $qty;
                        $cart->save();

                        return json_encode(array("status" => 0, "cart" => $cart));
                    
                    }
                }
                
            }else{
                return json_encode(array("status"=> 2, "status" => "Token Mismatch"));
            } 
        }


        return json_encode(array("status"=> 3));
    }


    /* /product/cart/checkout */
    function _CheckoutCart(Request $req){
        $token          = $req->get('token');
        $user_id        = $req->get('user_id');
        $verification_pin = $req->get('verification_pin');

        $user           = User::find($user_id);

        if($user != null){
            if($token == $user->login_token){
                $kigspay = Kigspay::where('user_id', $user_id)->first();

                if(Hash::check($verification_pin, $kigspay->verification_pin)){

                $carts                          = Cart::where("user_id", $user_id)->where("status", 0)->get();
                $total_amount                   = 0;
                
                    if($carts != null){
                        foreach($carts as $cart){
                            $total_amount += ($cart->getProduct->price * $cart->qty);
                        }

                        $transaction                    = New Transaction();
                        $transaction->user_id           = $user_id;
                        $transaction->type              = 1; //Purchase Through Cart
                        $transaction->date              = \Carbon\Carbon::now();
                        $transaction->payment_type      = 1; //Kigspay 
                        $transaction->amount            = $total_amount;
                        
                        
                        if($transaction->save()){
                            foreach($carts as $cart){
                                $purchasedProduct = new PurchasedProduct();
                                $purchasedProduct->transaction_id = $transaction->id;
                                $purchasedProduct->product_id     = $cart->getProduct->id;
                                $purchasedProduct->qty            = $cart->qty;
                                $purchasedProduct->total_price    = $cart->qty * $cart->getProduct->price;
                                $purchasedProduct->save();
                                $cart->delete();
                            }
                        }

                        return json_encode(array("status" => 0, "transaction" => $transaction));
                    }

                }else{
                    return json_encode(array("status"=> 4, "message" => "Pin is not Correct"));
                }
            }else{
                return json_encode(array("status"=> 2, "message" => "Token Mismatch"));
            }
        }else{
            return json_encode(array("status"=> 2));
        }

        return json_encode(array("status"=> 3));    
    }

    
    function _KigspayMakePayment(Request $req){

    }

    function _AddProduct(Request $req){
        $name       = $req->get('name');
        $barcode    = $req->get('barcode');
        $price      = $req->get('price');
        $qty        = $req->get('qty');
        
        $product    = new Product();
        $product->name      = $name;
        $product->barcode   = $barcode;
        $product->price     = $price;
        $product->qty       = $qty;
        

        if($product->save()){
            return json_encode(array("status" => 0, "product" => new ProductResource($product)));
        }else{
            return json_encode(array("status" => 2));
        }
    }

    
    function _GetAllProduct(Request $req){
        $products = Product::all();
        return json_encode(array("products" => new ProductResource($products)));
    }

    function _GetProductByBarcode($barcode){
        $product = Product::where('barcode', $barcode)->first();

        return json_encode(array("product" => new ProductResource($product)));
    }



    function _PaymentManualAmount(Request $req){
        $type = 3;  // 3 is Manual Payment Amount
        $payment_type = 3;
        $amount = $req->get('amount');
        
        $transaction = new Transaction();
    }
}
