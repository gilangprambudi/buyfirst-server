<?php 
Route::post('/authenticate/user', 'APIController@_AuthenticateUser');
Route::post('/authenticate/user/token', 'APIController@_CheckTokenIntegrity');
Route::post('/authenticate/user/verification-pin', 'APIController@_CheckVerificationPin');


Route::post('/register/user', 'APIController@_RegisterUser');
Route::post('/register/user/verification_pin', 'APIController@_RegisterVerificationPin');
Route::post('/product/add/cart/qrcode', 'APIController@_AddProductToCartByQRCode');
Route::post('/product/cart/checkout', 'APIController@_CheckoutCart');

Route::get('/product/get/cart', 'APIController@_GetCart');

Route::post("/product/update/cart", "APIController@_ModifyCart");

Route::post("/product/add", "APIController@_AddProduct");
Route::get("/product", "APIController@_GetAllProduct");
Route::get("/product/{barcode}", "APIController@_GetProductByBarcode");

Route::post("/transaction/pay-manual-amount", "APIController@_PaymentManualAmount");


// Route::get('/authenticate/user', function () {
//     return new UserResource(User::find(1));
// });
